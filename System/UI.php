<?php   

  class UI{

  	public function __construct(){

  	  $flag = false;
      
      //router
  	  if(isset($_GET['path'])){

  	  	$tokens = explode('/', rtrim($_GET['path'], '/'));

  	  	//dispatcher
  	  	$controllerName = ucfirst(array_shift($tokens));

  	  	if(file_exists('Controllers/'.$controllerName.'.php')){

  	  		$controller = new $controllerName();

  	  		//run an action
  	  		if(!empty($tokens)){

  	  			$actionName = array_shift($tokens);

  	  			if(method_exists($controller, $actionName)){

  	  				//passing parameter if exists or null if not
  	  				$controller->{$actionName}(@$tokens);

  	  			}else{

  	  				//if action not found error page
  	  				$flag = true;


  	  			}
  	  		
  	  		}else{

  	  			//default action
  	  			$controller->index();

  	  		}


  	  	}else{

  	  		//if controller not found render an error page

  	  		$flag = true;




  	  	}

  	  }else{

  	  	//if no controller entered

  	  	//name a unique name controller that can be in a system and can not named in a controller
  	  	$controllerName = 'HomeController';

  	  	$controller = new $controllerName();
  	  	$controller->index(); 

  	  }


  	  //Error page
  	  if($flag){

  	  	$controllerName = 'Error404PageNotFoundController';
  	  	$controller = new $controllerName();

  	  	$controller->index();
  	  }

  	}

  	
  }